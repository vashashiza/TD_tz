﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Source
{
    public static IEnumerator CyclicAction(YieldInstruction yieldInstruction, Func<bool> action)
    {
        do
        {
            yield return yieldInstruction;
        } while (action());
        yield break;
    }
}

public abstract class MonoBehaviourSinglton<T> : MonoBehaviour where T : MonoBehaviourSinglton<T>
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<T>();

                if (instance == null)
                {
                    GameObject gameObject = new GameObject(typeof(T).ToString());
                    instance = gameObject.AddComponent<T>();
                }
            }
            return instance;
        }

        protected set
        {
            if (value == null)
            {
                instance = null;
            }
            else
            {
                if (instance == null)
                {
                    instance = value;
                }
                else if (value != instance)
                {
                    Destroy(value);
                }
            }
        }
    }

    protected virtual void Awake()
    {
        Instance = this as T;
    }
}

public abstract class ScriptableObjectSinglton<T> : ScriptableObject where T : ScriptableObjectSinglton<T>
{
    private static T instanse;
    public static T Instanse
    {
        get
        {
            if (instanse == null)
            {
                string name = typeof(T).ToString();

                instanse = Resources.Load<T>(name);

                if (instanse == null)
                {
                    instanse = CreateAsset(name);
                }
            }
            return instanse;
        }       
    }

    public static T CreateAsset(string name, string filePath = "Assets/Resources/")
    {
        T scriptableObject = CreateInstance<T>();

#if UNITY_EDITOR
        if (!Directory.Exists(filePath)) Directory.CreateDirectory(filePath);
        UnityEditor.AssetDatabase.CreateAsset(scriptableObject, string.Format("{0}{1}.asset", filePath, name));
        UnityEditor.AssetDatabase.SaveAssets();
#endif
        return scriptableObject;
    }
}

public enum AttackType { Freezing }
