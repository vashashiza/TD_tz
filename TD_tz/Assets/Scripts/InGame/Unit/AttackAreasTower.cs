﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAreasTower : Tower
{
    [SerializeField]
    protected LayerMask layerMask;
    [SerializeField]
    protected float effectsValue = 2;
    [SerializeField]
    protected AttackType attackType;
    [SerializeField]
    protected float activeDamagesTime;
   
}
