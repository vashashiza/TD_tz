﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : Bullet
{
    [SerializeField]
    protected float attackRadius = 5;
    [SerializeField]
    protected LayerMask layerMask;

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, attackRadius, layerMask);
            foreach (Collider item in colliders)
            {
                Enemy enemy = item.gameObject.GetComponent<Enemy>();
                if (enemy != null)
                {
                    float distance = Vector3.Distance(transform.position, enemy.transform.position);                    
                    enemy.SetDamage(damage - distance);
                }
            }
            Disintegrate();
        }
    }


}
