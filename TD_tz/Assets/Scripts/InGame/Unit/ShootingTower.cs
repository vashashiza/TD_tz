﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingTower : Tower
{
    [SerializeField]
    protected Bullet bulletPrefab = null;

    protected virtual void spawnBullet()
    {
        Bullet bullet = Instantiate(bulletPrefab, transform.position, transform.rotation);
        bullet.Target = Targets[0];
    }

    protected override void shot()
    {
        base.shot();
        spawnBullet();
    }
}
