﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezingTower : AttackAreasTower
{
    protected override void shot()
    {
        foreach (Enemy item in Targets)
        {
            if (item != null)
            {
                item.SetContinuousDamage(attackType, effectsValue, activeDamagesTime);
            }
        }
    }
}