﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingEnemy : Enemy
{
    [SerializeField]
    protected float attackRadius = 5;
    [SerializeField]
    protected float healingCount = 3;
    [SerializeField]
    protected LayerMask layerMask;

    protected virtual void healingBeforeDeath()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, attackRadius, layerMask);
        foreach (Collider item in colliders)
        {
            Enemy enemy = item.gameObject.GetComponent<Enemy>();
            if (enemy != null && enemy != this)
            {
                enemy.HitPoints += healingCount; 
            }
        }
    }

    protected override void death()
    {
        healingBeforeDeath();
        base.death();
    }
}
