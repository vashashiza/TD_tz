﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IPullable
{
    UnitType UnitType { get; }    

    void Off();

    void Reset();
}

public enum UnitType
{
    Enemy,
    AcceleratingEnemy,
    HealingEnemy,
    Tower
}
