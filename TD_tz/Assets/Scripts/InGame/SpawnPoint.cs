﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    [SerializeField]
    private List<Transform> Routes = new List<Transform>();
    [SerializeField]
    private List<List<Transform>> RoutesList = new List<List<Transform>>();

    public List<Transform> ReturnRandomRoute()
    {
       int randomRoute = Random.Range(0, RoutesList.Count);
       return RoutesList[randomRoute];
    }

    private void Awake()
    {
        getChildrenList();
       
    }


    private void getChildrenList()
    {
        foreach (Transform item in Routes)
        {
            List<Transform> children = new List<Transform>();
            for (int i = 0; i < item.childCount; i++)
            {
                children.Add(item.GetChild(i));
            }
            RoutesList.Add(children);
        }
    }


}
