﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : SellUnit, IPullable
{
    private List<Transform> movePoints = new List<Transform>();

    public event Action<Enemy> OnCame;
    public event Action<Enemy> OnMurder;

    [SerializeField]
    protected bool isAlive = true;

    [SerializeField]
    protected float baseMoveSpeed = 10;

    [SerializeField]
    protected int movePointIterator = 0;

    protected float moveSpeed;

    protected Dictionary<AttackType, Coroutine> effects = new Dictionary<AttackType, Coroutine>();

    [SerializeField]
    protected float baseHitPoints = 10;
    [SerializeField]
    protected float hitPoints = 10;
    public virtual float HitPoints
    {
        get
        {
            return hitPoints;
        }

        set
        {
            if (isAlive)
            {
                hitPoints = value;
                if (hitPoints <= 0)
                {
                    death();
                }
            }
        }
    }
     
    public float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }

        set
        {
            moveSpeed = value;
        }
    }

    public List<Transform> MovePoints
    {
        get
        {
            return movePoints;
        }

        set
        {
            movePoints = value;
        }
    }

    [SerializeField]
    protected UnitType unitType;
    public UnitType UnitType
    {
        get
        {
            return unitType;
        }
    }

    public virtual void SetDamage(float damage)
    {
        if (this != null)
        {
            HitPoints -= damage;
        }
    }

    public virtual void SetContinuousDamage(AttackType attackType, float effectsValue, float activeDamagesTime, float delayTime = 0)
    {
        if ((object)this != null)
        {
            Coroutine effectCoroutine; 
            if (effects.TryGetValue(attackType, out effectCoroutine))
            {
                effects.Remove(attackType);
                StopCoroutine(effectCoroutine);
            }
            switch (attackType)
            {
                case AttackType.Freezing:
                    effectCoroutine = StartCoroutine(Freeze(activeDamagesTime, effectsValue));
                    break;
            }

            effects.Add(attackType, effectCoroutine);
        }
    }

    protected IEnumerator Freeze(float activeDamagesTime, float effectsValue)
    {
        MoveSpeed = effectsValue;

        yield return new WaitForSeconds(activeDamagesTime);

        MoveSpeed = baseMoveSpeed;
        yield break;
    }

    //дляштук вроде отравления: минус сколькото хитов раз в период
    protected IEnumerator continuousDamage(float damage, float delayTime, float finishTime)
    {
        while (Time.time < finishTime)
        {
            SetDamage(damage);
            yield return new WaitForSeconds(delayTime);
        }
    }

    protected virtual void death()
    {
        isAlive = false;
        riseOnMurder();
        Disintegrate();
    }

    protected virtual void riseOnCame()
    {
        if (OnCame != null) OnCame(this);
    }

    protected virtual void riseOnMurder()
    {
        if (OnMurder != null) OnMurder(this);
    }

    protected virtual void move()
    {
        float step = MoveSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, MovePoints[movePointIterator].position, step);
    }

    protected virtual void checkRoute()
    {
        if (transform.position == MovePoints[movePointIterator].position)
        {
            if (movePointIterator == MovePoints.Count - 1)
            {
                riseOnCame();
                Disintegrate();
            }
            else
            {
                movePointIterator++;
            }
        }
        move();
    }

    protected virtual void Update()
    {
        checkRoute();
    }

    protected virtual void Start()
    {
        moveSpeed = baseMoveSpeed;
    }

    public void Reset()
    {
        //gameObject.SetActive(false);
        moveSpeed = baseMoveSpeed;
        hitPoints = baseHitPoints;
        isAlive = true;
    }

    public void Off()
    {
        gameObject.SetActive(false);
    }

    protected override void clearEvents()
    {
        base.clearEvents();
        OnMurder = null;
        OnCame = null;        
    }
}