﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Unit
{
    [SerializeField]
    protected float damage = 2;
    [SerializeField]
    protected float moveSpeed = 2;
    [SerializeField]
    protected float range = 200;
    protected Vector3 startPosition = new Vector3();

    private Enemy target = null;
    public Enemy Target
    {
        get
        {
            return target;
        }

        set
        {
            target = value;
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            enemy.SetDamage(damage);
            Disintegrate();
        }
    }

    protected virtual void Start()
    {
        startPosition = transform.position;
    }

    protected virtual void Update()
    {
        move();
    }

    protected virtual void move()
    {
        if (target != null)
        {
            float step = moveSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
            if (Vector3.Distance(transform.position, startPosition) >= range)
            {
                Disintegrate();
            }
        }
        else Disintegrate();

    }
}