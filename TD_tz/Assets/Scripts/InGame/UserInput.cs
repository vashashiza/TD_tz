﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UserInput : MonoBehaviourSinglton<UserInput>
{
    [SerializeField]
    private float maxDistanceCheck = 2000;

    [SerializeField]
    private List<Tower> towerPrefabs = null;
    [SerializeField]
    private List<Text> buttonTexts = new List<Text>();

    [SerializeField]
    private LayerMask layerMask = new LayerMask();

    private Coroutine constructCoroutine;
    private Tower towerConstruct;

    public virtual void ButtonClick(int index)
    {
        if (constructCoroutine == null)
        {
            float moneyBalance = InGameUI.Instance.Money - towerPrefabs[index].UnitCost;
            if (moneyBalance >= 0)
            {
                towerConstruct = Instantiate(towerPrefabs[index]);
                constructCoroutine = StartCoroutine(setTowerRoutine());
            }
        }
    }

    public void StateTowerMenu(Tower tower, bool activeState)
    {       
       tower.TowersPanel.gameObject.SetActive(activeState);
    }

    private IEnumerator setTowerRoutine()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && towerConstruct != null)
            {
                towerConstruct.Disintegrate();
                towerConstruct = null;
                break;
            }

            if (IsConstructedPlace())
            {
                if (Input.GetMouseButton(0))
                {
                    towerConstruct.Construct();
                    //InGameUI.Instance.CreateTowerUIPanel(towerConstruct);
                    InGameUI.Instance.UpdateMoneyCounter(-towerConstruct.UnitCost);
                    towerConstruct = null;
                    break;
                }
            }
            yield return null;
        }
        constructCoroutine = null;
    }

    public bool IsConstructedPlace()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, maxDistanceCheck, layerMask))
        {
            towerConstruct.transform.position = hit.point;
            if (hit.collider.tag == "ConstructedPlace")
            {
                return true;
            }
        }
        return false;
    }

    public bool IsTower(out Tower tower)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag == "Tower")
            {
                tower = hit.collider.gameObject.GetComponent<Tower>();
                return true;
            }
        }
        tower = null;
        return false;
    }

    private void Start()
    {
        for (int i = 0; i < towerPrefabs.Count; i++)
        {
            buttonTexts[i].text += " " + towerPrefabs[i].UnitCost + "$";
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && towerConstruct == null)
        {
            Tower tower;
            if (IsTower(out tower))
            {
                InGameUI.Instance.UpdateMoneyCounter(tower.UnitCost);
                tower.Disintegrate();                
            }
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }
}
