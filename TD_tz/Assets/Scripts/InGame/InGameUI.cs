﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviourSinglton<InGameUI>
{

  /*  private bool haveTowers = false;
    public bool HaveTowers
    {
        get
        {
            return haveTowers;
        }

        set
        {
            haveTowers = value;
        }
    }
    */
    public Text GameOverText = null;
    public Text WinText = null;

    [SerializeField]
    private TowersPanel towersPanelPrefab = null;
    [SerializeField]
    private GameObject canvas = null;


    [SerializeField]
    private Text moneyCounter = null;
    [SerializeField]
    private Text enemysCounter = null;

    public float Money;
    private int allEnemys;
    private int maxLoseEnemys;

    public void UpdateEnemysCounter(int killed, int lose)
    {
        enemysCounter.text = "All: " + allEnemys + "; Killed: " + killed + "; To Lose: " + lose + "/" + maxLoseEnemys;
    }

   /* public void CreateTowerUIPanel(Tower towerTarget)
    {
        TowersPanel towersPanel = Instantiate(towersPanelPrefab);
        towersPanel.transform.SetParent(canvas.transform);
        towersPanel.transform.position = Camera.main.WorldToScreenPoint(new Vector3(towerTarget.transform.position.x, towerTarget.transform.position.y, towerTarget.transform.position.z));
        towerTarget.TowersPanel = towersPanel;
        towersPanel.Target = towerTarget;
    }*/

    public void UpdateMoneyCounter(float cost)
    {
        Money += cost;
        moneyCounter.text = "Money: " + Money + "$";
    }

    public void SetUIInformation(int enemys, float startMoney, int gameOverLoseEnemysCount)
    {
        allEnemys = enemys;
        Money = startMoney;
        maxLoseEnemys = gameOverLoseEnemysCount;

        UpdateEnemysCounter(0, 0);
        UpdateMoneyCounter(0);
    }

    public void OnDestroy()
    {
        Instance = null;
    }
}