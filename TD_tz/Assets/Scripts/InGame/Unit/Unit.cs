﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{  
    public event Action<Unit> OnDisintegrate;
    protected virtual void riseOnDisintegrate()
    {
        if (OnDisintegrate != null) OnDisintegrate(this);
    }

    public virtual void Disintegrate()
    {
        if ((object)this != null)
        {
            Destroy(gameObject);
            riseOnDisintegrate();
        }
    }

    protected virtual void clearEvents()
    {
        OnDisintegrate = null;
    }
}
