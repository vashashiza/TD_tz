﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tower : SellUnit
{
    public List<Enemy> Targets = new List<Enemy>();

    [SerializeField]
    protected Rigidbody thisRigidbody;
    [SerializeField]
    protected SurveyTowerCollider surveyColliderGameObject = null;
    [SerializeField]
    protected SphereCollider uITowerCollider = null;

    [SerializeField]
    protected float reloadTime = 1;


   /* protected bool isActivePanel = false;
    public bool IsActivePanel
    {
        get
        {
            return isActivePanel;
        }

        set
        {
            isActivePanel = value;
            if (isActivePanel)
            {

            }
            else
            {
                    
            }
        }
    }

    protected bool isClicked = true;
    protected void onClick()
    {
        isClicked = !isClicked;
        UserInput.Instance.StateTowerMenu(this, isClicked);
    }
    */

    protected bool isReload = true;
    public bool IsReload
    {
        get
        {
            return isReload;
        }
        set
        {
            isReload = value;
            if (!value)
            {
                StartCoroutine(reload());
            }
        }
    }

    [SerializeField]
    protected TowersPanel towersPanel = null;
    public TowersPanel TowersPanel
    {
        get
        {
            return towersPanel;
        }

        set
        {
            towersPanel = value;
        }
    }

    protected void Awake()
    {
        surveyColliderGameObject.OnEnemyCame += SurveyColliderGameObject_OnEnemyCame;
        surveyColliderGameObject.OnEnemyExit += SurveyColliderGameObject_OnEnemyExit;
    }

    private void SurveyColliderGameObject_OnEnemyExit(Enemy obj)
    {
        Targets.Remove(obj);
    }

    private void SurveyColliderGameObject_OnEnemyCame(Enemy obj)
    {
        Targets.Add(obj);
        obj.OnDisintegrate += Enemy_OnDisintegrate;
    }

    public void Construct()
    {
        thisRigidbody.isKinematic = true;
        surveyColliderGameObject.SurveyCollider.enabled = true;
    }

    protected virtual IEnumerator reload()
    {
        yield return new WaitForSeconds(reloadTime);
        IsReload = true;
    }
    protected virtual void Enemy_OnDisintegrate(Unit sender)
    {
        Targets.Remove(sender as Enemy);
    }
 
    protected virtual void shot()
    {

    }

    /*public void OnMouseDown()
    {
        Debug.Log("aa");
        onClick();
    }*/

    protected virtual void Update()
    {
        if (isReload && Targets.Count > 0)
        {
            IsReload = false;
            shot();
        }
    }


}
