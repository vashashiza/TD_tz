﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcceleratingEnemy : Enemy
{
    [SerializeField]
    protected float criticalHitPoints = 5;

    [SerializeField]
    protected float speedFactor = 1.5f;

    public override float HitPoints
    {
        get
        {
            return hitPoints;
        }
        set
        {
            if (value <= criticalHitPoints)
            {
                MoveSpeed = baseMoveSpeed * speedFactor;
            }
            else
            {
                MoveSpeed = baseMoveSpeed;
            }

            base.HitPoints = value;            
        }
    }
}
