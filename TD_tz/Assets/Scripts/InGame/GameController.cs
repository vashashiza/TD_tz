﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{    
    [SerializeField]
    private List<Enemy> enemyPrefabs = new List<Enemy>();

    [SerializeField]
    private List<SpawnPoint> spawnPoints = null;

    [SerializeField]
    private int maxLoseEnemys = 10;
    [SerializeField]
    private float spawnEnemysDelay = 3;
    [SerializeField]
    private int enemysForLevelCount = 10;

    [SerializeField]
    private int startMoney = 10;

    private int loseEnemys = 0;
    private int killed = 0;

    public int EnemysForLevelCount
    {
        get
        {
            return enemysForLevelCount;
        }
        set
        {           
            enemysForLevelCount = value;
            if (enemysForLevelCount == 0)
            {                
                 win();
            }
        }
    }

    private void win()
    {
        Time.timeScale = 0;
        InGameUI.Instance.WinText.gameObject.SetActive(true);
    }

    private void spawnEnemys(Enemy enemyPrefab)
    {
        int spawnPointIndex = UnityEngine.Random.Range(0, spawnPoints.Count);
        List<Transform> routeList = spawnPoints[spawnPointIndex].ReturnRandomRoute();
        
        Enemy enemy = Pull.Instance.Get<Enemy>(enemyPrefab.UnitType);
        enemy.transform.position = routeList[0].position;

        enemy.MovePoints = routeList;

        enemy.gameObject.SetActive(true);

        enemy.OnCame += Enemy_OnCame;
        enemy.OnMurder += Enemy_OnMurder;
    }

    private void Enemy_OnMurder(Enemy sender)
    {
        killed++;

        InGameUI.Instance.UpdateEnemysCounter(killed, loseEnemys);       
        InGameUI.Instance.UpdateMoneyCounter(sender.UnitCost);

        EnemysForLevelCount--;
    }

    IEnumerator spawnEnemysWithDelay(int count)
    {
        for (int i = 0; i < count; i++)
        {
            int index = UnityEngine.Random.Range(0, enemyPrefabs.Count);
            spawnEnemys(enemyPrefabs[index]);

            yield return new WaitForSeconds(spawnEnemysDelay);
        }
    }

    public void Start()
    {
        StartCoroutine(spawnEnemysWithDelay(EnemysForLevelCount));
        InGameUI.Instance.SetUIInformation(enemysForLevelCount, startMoney, maxLoseEnemys);
    }

    private void Enemy_OnCame(Enemy sender)
    {
        loseEnemys++;
        if (loseEnemys == maxLoseEnemys)
        {
            gameOver();
        }

        InGameUI.Instance.UpdateEnemysCounter(killed, loseEnemys);

        EnemysForLevelCount--;
    }

    private void gameOver()
    {
        Time.timeScale = 0;
        InGameUI.Instance.GameOverText.gameObject.SetActive(true);
    }
}