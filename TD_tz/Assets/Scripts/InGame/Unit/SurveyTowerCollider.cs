﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SurveyTowerCollider : MonoBehaviour
{
    public SphereCollider SurveyCollider = null;
    public event Action<Enemy> OnEnemyCame;
    public event Action<Enemy> OnEnemyExit;

    protected virtual void riseOnCame(Enemy enemy)
    {
        if (OnEnemyCame != null) OnEnemyCame(enemy);
    }

    protected virtual void riseOnExit(Enemy enemy)
    {
        if (OnEnemyExit != null) OnEnemyExit(enemy);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            riseOnCame(enemy);
        }
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            riseOnExit(enemy);
        }
    }


}